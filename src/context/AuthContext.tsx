//context/AuthContext.tsx

import { createContext, useContext, useState, useEffect, ReactNode } from 'react';

export interface User {
  accessToken: string;
  trainerId: number;
  expiresAt: number;
}

interface AuthContextType {
  user: User | null;
  login: (accessToken: string, trainerId: number, expiresIn: number) => void;
  logout: () => void;
}

export const AuthContext = createContext<AuthContextType | undefined>(undefined);

interface AuthProviderProps {
  children: ReactNode;
}

export const AuthProvider = ({ children }: AuthProviderProps) => {
  const [user, setUser] = useState<User | null>(() => {
    const accessToken = localStorage.getItem('accessToken');
    const trainerId = localStorage.getItem('trainerId');
    const expiresAt = localStorage.getItem('expiresAt');
    if (accessToken && trainerId && expiresAt) {
      return {
        accessToken,
        trainerId: parseInt(trainerId),
        expiresAt: parseInt(expiresAt),
      };
    }
    return null;
  });

  const login = (accessToken: string, trainerId: number, expiresIn: number) => {
    const expiresAt = new Date().getTime() + expiresIn * 1000; // expiresIn in seconds
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('trainerId', trainerId.toString());
    localStorage.setItem('expiresAt', expiresAt.toString());

    setUser({ accessToken, trainerId, expiresAt });
  };

  const logout = () => {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('trainerId');
    localStorage.removeItem('expiresAt');
    setUser(null);
  };

  // Check the expiry time of the token, if expired, logout the user
  useEffect(() => {
    const intervalId = setInterval(() => {
      if (user && new Date().getTime() > user.expiresAt) {
        logout();
      }
    }, 1000); // Check every second

    // Clear interval on cleanup
    return () => clearInterval(intervalId);
  }, [user]);

  return (
    <AuthContext.Provider value={{ user, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = () => {
  const context = useContext(AuthContext);
  if (!context) {
    throw new Error('useAuth must be used within an AuthProvider');
  }
  return context;
};

export default AuthProvider;
