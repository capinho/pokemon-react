// App.tsx

import { Routes, Route, Navigate } from 'react-router-dom';
import Header from './components/Header';
import HomePage from './pages/HomePage';
import LoginPage from './pages/LoginPage';
import SubscribePage from './pages/SubscribePage';
import BoxesPage from './pages/BoxesPage';
import TradesPage from './pages/TradesPage';
import SearchTrainersPage from './pages/SearchTrainersPage';
import SearchPokemonsPage from './pages/SearchPokemonsPage';
import UserProfilePage from './pages/UserProfilePage';
import PrivateRoute from './components/PrivateRoute';
import CreateBoxPage from './pages/CreateBoxPage';
import BoxDetail from './pages/BoxDetail';
import CreatePokemonPage from './pages/CreatePokemonPage';
import PokemonDetailPage from './pages/PokemonDetailPage';
import EditPokemonPage from './pages/EditPokemonPage'; 
import CreateTradePage from './pages/CreateTradePage';
import TradeDetailPage from './pages/TradeDetailPage';
import UserEditPage from './pages/UserEditPage';
import Footer from './components/Footer';
import AboutPage from './pages/AboutPage';
import RenameBoxPage from './pages/RenameBoxPage';
import DeleteBoxPage from './pages/DeleteBoxPage';
import MovePokemonPage from './pages/MovePokemonPage';

function App() {
  return (
    <div className='h-[100vh] flex flex-col'>
      <Header />
      <div className='flex-grow'>
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/login" element={<LoginPage />} />
        <Route path="/subscribe" element={<SubscribePage />} />
        <Route path="/" element={<PrivateRoute />}>
          <Route index element={<Navigate to="/boxes" />} />
          <Route path="/boxes" element={<BoxesPage />} />
          <Route path="/boxes/:boxId" element={<BoxDetail />} />
          <Route path="/create-box" element={<CreateBoxPage />} />
          <Route path="/rename-box/:boxId" element={<RenameBoxPage />} />
          <Route path="/delete-box/:boxId" element={<DeleteBoxPage />} />
          <Route path="/move-pokemon/:pokemonId" element={<MovePokemonPage />} />
          <Route path="/add-pokemon" element={<CreatePokemonPage />} />
          <Route path="/pokemons/:pokemonId/edit" element={<EditPokemonPage />} />
          <Route path="/pokemons/:pokemonId" element={<PokemonDetailPage />} />
          <Route path="/trades" element={<TradesPage />} />
          <Route path="/create-trade" element={<CreateTradePage />} />
          <Route path="/trades/:tradeId" element={<TradeDetailPage />} />
          <Route path="/search-trainers" element={<SearchTrainersPage />} />
          <Route path="/search-pokemons" element={<SearchPokemonsPage />} />
          <Route path="/profile" element={<UserProfilePage />} />
          <Route path="/profile/:trainerId" element={<UserProfilePage />} />
          <Route path="/user-edit" element={<UserEditPage />} />
          <Route path="/about" element={<AboutPage />} />
        </Route>
      </Routes>

      </div>
      <Footer />

    </div>
  );
}

export default App;
