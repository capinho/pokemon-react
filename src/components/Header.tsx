//components/Header.tsx

import { Link } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { FaBoxOpen, FaExchangeAlt, FaSearch, FaUserCircle, FaSignInAlt, FaUserPlus, FaSignOutAlt } from 'react-icons/fa';

function Header() {
  const { user, logout } = useAuth();

  return (
    <header className="bg-gray-100 text-gray-800 shadow-md">
      <nav className="container mx-auto flex justify-between items-center p-4">
        <Link to="/" className="flex items-center">
          <img src="src/assets/pokemon-logo.png" alt="Pokemon Logo" className="h-8 mr-2" />
        </Link>

        <ul className="flex items-center space-x-4">
          {user ? (
            <>
              {/* User logged in links */}
              <li><Link to="/boxes" className="flex items-center hover:text-blue-600"><FaBoxOpen className="mr-1" /> Mes boîtes</Link></li>
              <li><Link to="/trades" className="flex items-center hover:text-blue-600"><FaExchangeAlt className="mr-1" /> Mes échanges</Link></li>
              <li><Link to="/search-trainers" className="flex items-center hover:text-blue-600"><FaSearch className="mr-1" /> Chercher Dresseurs</Link></li>
              <li><Link to="/search-pokemons" className="flex items-center hover:text-blue-600"><FaSearch className="mr-1" /> Chercher Pokémon</Link></li>
              <li><Link to="/profile" className="flex items-center hover:text-blue-600"><FaUserCircle className="mr-1" /> Profil</Link></li>
              <li>
                <button onClick={logout} className="flex items-center hover:text-red-600">
                  <FaSignOutAlt className="mr-1" /> Déconnexion
                </button>
              </li>
            </>
          ) : (
            <>
              {/* User not logged in links */}
              <li><Link to="/login" className="flex items-center hover:text-blue-600"><FaSignInAlt className="mr-1" /> Se connecter</Link></li>
              <li><Link to="/subscribe" className="flex items-center hover:text-blue-600"><FaUserPlus className="mr-1" /> S'inscrire</Link></li>
            </>
          )}
        </ul>
      </nav>
    </header>
  );
}

export default Header;
