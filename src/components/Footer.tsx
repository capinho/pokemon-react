// components/Footer.tsx
import { Link } from 'react-router-dom';

function Footer() {
  return (
    <footer className="bg-gray-800 text-white py-6 mt-auto">
      <nav className="container mx-auto px-4 text-center">
        <Link to="/about" className="hover:text-blue-300">À propos</Link>
      </nav>
    </footer>
  );
}

export default Footer;
