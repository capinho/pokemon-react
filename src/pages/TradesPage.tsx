// pages/TradesPage.tsx
import { useEffect, useState, useCallback } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { Trade, UserProfile } from '../Types';

function TradesPage() {
  const { user,logout } = useAuth();
  const navigate = useNavigate();
  const [trades, setTrades] = useState<Trade[]>([]);
  const [trainerDetails, setTrainerDetails] = useState<{ [key: number]: UserProfile }>({});
  const [page, setPage] = useState(0);
  const pageSize = 20;
  const [orderBy, setOrderBy] = useState('DESC');
  const [statusCode, setStatusCode] = useState('');


    // État pour les sélections de Pokémon
    const [selectedPokemonsForReceiver, setSelectedPokemonsForReceiver] = useState<number[]>([]);
    const [selectedPokemonsForSender, setSelectedPokemonsForSender] = useState<number[]>([]);
  
  const fetchTrainerDetails = useCallback(async (trainerId:number) => {
    if (!trainerDetails[trainerId] && user?.accessToken) {
      try {
        const response = await fetch(`http://localhost:8000/trainers/${trainerId}`, {
          headers: { 'Authorization': `Bearer ${user.accessToken}` },
        });
        if (!response.ok) {
          throw new Error('Failed to fetch trainer details');
        }
        const data = await response.json();
        setTrainerDetails((prevDetails) => ({ ...prevDetails, [trainerId]: data }));
      } catch (error) {
        console.error('Error fetching trainer details:', error);
      }
    }
  }, [user?.accessToken, trainerDetails]);

  useEffect(() => {
    const fetchTrades = async () => {
      try {
        const queryParams = new URLSearchParams({
          page: page.toString(),
          pageSize: pageSize.toString(),
          orderBy,
          ...(statusCode && { statusCode }),
        });

        const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/trades?${queryParams}`, {
          headers: { 'Authorization': `Bearer ${user?.accessToken}` },
        });
        if (!response.ok) throw new Error('Failed to fetch trades');

        const data = await response.json();
        setTrades(data);
      } catch (error) {
        if (error instanceof Error && error.message === 'Failed to fetch trades') {
          logout()
          navigate('/login');
        } else {
          console.error('Error:', error);
        }
      }
    };

    fetchTrades();
  }, [user?.trainerId, user?.accessToken, page, pageSize, orderBy, statusCode, logout, navigate]);

  useEffect(() => {
    trades.forEach(trade => {
      fetchTrainerDetails(trade.sender.id);
      fetchTrainerDetails(trade.receiver.id);
    });
  }, [trades, fetchTrainerDetails]);

  const handleNavigateToTradeDetail = (tradeId:number) => {
    navigate(`/trades/${tradeId}`);
  };

  return (
        <div className="container mx-auto px-4 py-8">
        <h1 className="text-2xl font-bold mb-6">Liste des Échanges</h1>

        {/* Filtres */}
        <div className="flex gap-4 mb-6">
            <select 
                value={statusCode} 
                onChange={(e) => setStatusCode(e.target.value)}
                className="block w-full py-2 px-4 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500"
            >
                <option value="">Tous les statuts</option>
                <option value="PROPOSITION">Proposition</option>
                <option value="ACCEPTED">Accepté</option>
                <option value="DECLINED">Refusé</option>
            </select>

            <select 
                value={orderBy} 
                onChange={(e) => setOrderBy(e.target.value)}
                className="block w-full py-2 px-4 border border-gray-300 rounded-md shadow-sm focus:outline-none focus:ring-blue-500 focus:border-blue-500"
            >
                <option value="DESC">Du plus récent au plus ancien</option>
                <option value="ASC">Du plus ancien au plus récent</option>
            </select>
        </div>

        {/* Liste des échanges */}
        <table className="min-w-full table-auto bg-white rounded-lg shadow-md overflow-hidden">
            <thead className="bg-gray-100">
                <tr>
                    <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Statut</th>
                    <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Receveur</th>
                    <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Envoyeur</th>
                    <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Actions</th>
                </tr>
            </thead>
            <tbody className="bg-white">
                {trades.map((trade) => (
                    <tr key={trade.id} className="border-b">
                        <td className="px-6 py-4 whitespace-nowrap">{trade.statusCode}</td>
                        <td className="px-6 py-4 whitespace-nowrap">{trainerDetails[trade.receiver.id]?.firstName} {trainerDetails[trade.receiver.id]?.lastName}</td>
                        <td className="px-6 py-4 whitespace-nowrap">{trainerDetails[trade.sender.id]?.firstName} {trainerDetails[trade.sender.id]?.lastName}</td>
                        <td className="px-6 py-4 whitespace-nowrap">
                            <button 
                                onClick={() => handleNavigateToTradeDetail(trade.id)}
                                className="text-indigo-600 hover:text-indigo-900"
                            >
                                Détails
                            </button>
                            {/* Autres boutons et logique ici */}
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>

        {/* Pagination */}
        <div className="flex justify-between items-center mt-6">
            <button 
                onClick={() => setPage((prev) => Math.max(prev - 1, 0))}
                disabled={page === 0}
                className="px-4 py-2 text-sm font-medium text-white bg-blue-500 rounded-md hover:bg-blue-700 disabled:opacity-50 disabled:cursor-not-allowed"
            >
                Précédent
            </button>
            <button 
                onClick={() => setPage((prev) => prev + 1)}
                className="px-4 py-2 text-sm font-medium text-white bg-blue-500 rounded-md hover:bg-blue-700"
            >
                Suivant
            </button>
        </div>
    </div>

  );
}

export default TradesPage;
