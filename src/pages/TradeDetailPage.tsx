// pages/TradeDetailPage.tsx
import { useEffect, useState,useCallback } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { Trade, UserProfile, Pokemon } from '../Types';
import { useAuth } from '../context/AuthContext';
import { FaUser, FaExchangeAlt, FaArrowLeft } from 'react-icons/fa';

function TradeDetailPage() {
  const { tradeId } = useParams<{ tradeId: string }>();
  const { user,logout } = useAuth();
  const navigate = useNavigate();
  const [trade, setTrade] = useState<Trade | null>(null);
  const [senderProfile, setSenderProfile] = useState<UserProfile | null>(null);
  const [receiverProfile, setReceiverProfile] = useState<UserProfile | null>(null);
  const [senderPokemons, setSenderPokemons] = useState<Pokemon[]>([]);
  const [receiverPokemons, setReceiverPokemons] = useState<Pokemon[]>([]);
  const [error, setError] = useState<string | null>(null);

  const fetchPokemonDetails = useCallback(async (pokemonIds: number[]) => {
    return Promise.all(pokemonIds.map(async (id) => {
      const response = await fetch(`http://localhost:8000/pokemons/${id}`, {
        headers: { 'Authorization': `Bearer ${user?.accessToken}` },
      });
      if (!response.ok) throw new Error(`Network response was not ok for pokemon ID ${id}`);
      return response.json();
    }));
  }, [user?.accessToken]);
  

  const handleAcceptTrade = async () => {
    try {
      const response = await fetch(`http://localhost:8000/trades/${tradeId}`, {
        method: 'PATCH',
        headers: {
          'Authorization': `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ statusCode: 'ACCEPTED' })
      });
  
      if (!response.ok) throw new Error('Failed to accept trade');
  
      // Mise à jour de l'état du trade après l'acceptation
      setTrade(prev => prev ? { ...prev, statusCode: 'ACCEPTED' } : null);
    } catch (error) {
      console.error('Error accepting trade:', error);
    }
  };
  
  const handleDeclineTrade = async () => {
    try {
      const response = await fetch(`http://localhost:8000/trades/${tradeId}`, {
        method: 'PATCH',
        headers: {
          'Authorization': `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ statusCode: 'DECLINED' })
      });
  
      if (!response.ok) throw new Error('Failed to decline trade');
  
      // Mise à jour de l'état du trade après le refus
      setTrade(prev => prev ? { ...prev, statusCode: 'DECLINED' } : null);
    } catch (error) {
      console.error('Error declining trade:', error);
    }
  };
  

  useEffect(() => {

    const fetchTradeDetails = async () => {
      try {

        const tradeResponse = await fetch(`http://localhost:8000/trades/${tradeId}`, {
          headers: { 'Authorization': `Bearer ${user?.accessToken}` },
        });
        if (!tradeResponse.ok) throw new Error('Network response was not ok for trade details');
        const tradeData: Trade = await tradeResponse.json();
        setTrade(tradeData);

        const senderData = await fetch(`http://localhost:8000/trainers/${tradeData.sender.id}`, {
          headers: { 'Authorization': `Bearer ${user?.accessToken}` },
        });
        if (!senderData.ok) throw new Error('Network response was not ok for sender profile');
        setSenderProfile(await senderData.json());

        const receiverData = await fetch(`http://localhost:8000/trainers/${tradeData.receiver.id}`, {
          headers: { 'Authorization': `Bearer ${user?.accessToken}` },
        });
        if (!receiverData.ok) throw new Error('Network response was not ok for receiver profile');
        setReceiverProfile(await receiverData.json());

        const senderPokemonDetails = await fetchPokemonDetails(tradeData.sender.pokemons);
        setSenderPokemons(senderPokemonDetails);

        const receiverPokemonDetails = await fetchPokemonDetails(tradeData.receiver.pokemons);
        setReceiverPokemons(receiverPokemonDetails);

      } catch (error) {
        if (error instanceof Error && error.message === 'Network response was not ok for trade details') {
          logout();
          navigate('/login');
        } else {
          console.error("Could not fetch trade details:", error);
          setError('Erreur lors de la récupération des détails de l\'échange');
        }
      }

    };

    fetchTradeDetails();
  }, [tradeId, user?.accessToken, fetchPokemonDetails, logout, navigate]);

  if (error) {
    return <div>Erreur: {error}</div>;
  }

  if (!trade || !senderProfile || !receiverProfile) {
    return <div>Chargement...</div>;
  }

  const handleBackClick = () => {
    navigate('/trades');
  };

  return (
    <div className="container mx-auto px-4 py-8">
    <h1 className="text-2xl font-bold text-blue-600 mb-6 flex items-center gap-2">
        <FaExchangeAlt />
        Détail de l'Échange
    </h1>

    {/* User Information */}
    <div className="mb-4 bg-white p-4 shadow rounded-lg">
        <h2 className="text-lg font-semibold flex items-center gap-2">
            <FaUser />
            Informations de l'Utilisateur
        </h2>
        <p className="text-gray-700">{senderProfile?.firstName} {senderProfile?.lastName} - {senderProfile?.login}</p>
    </div>

    {/* User Pokemons */}
    <div className="mb-4 bg-white p-4 shadow rounded-lg">
        <h3 className="text-md font-semibold">Pokémons de l'Utilisateur</h3>
        {senderPokemons.map((pokemon) => (
            <div key={pokemon.id} className="p-2 border-b">
                Espèce: {pokemon.species}, Nom: {pokemon.name}, Niveau: {pokemon.level}, Genre: {pokemon.genderTypeCode}, Chromatique: {pokemon.isShiny ? 'Oui' : 'Non'}
            </div>
        ))}
    </div>

    {/* Receiver Information */}
    <div className="mb-4 bg-white p-4 shadow rounded-lg">
        <h2 className="text-lg font-semibold flex items-center gap-2">
            <FaUser />
            Informations du Receveur
        </h2>
        <p className="text-gray-700">{receiverProfile?.firstName} {receiverProfile?.lastName} - {receiverProfile?.login}</p>
    </div>

    {/* Receiver Pokemons */}
    <div className="mb-4 bg-white p-4 shadow rounded-lg">
        <h3 className="text-md font-semibold">Pokémons du Receveur</h3>
        {receiverPokemons.map((pokemon) => (
            <div key={pokemon.id} className="p-2 border-b">
                Espèce: {pokemon.species}, Nom: {pokemon.name}, Niveau: {pokemon.level}, Genre: {pokemon.genderTypeCode}, Chromatique: {pokemon.isShiny ? 'Oui' : 'Non'}
            </div>
        ))}
    </div>

    {/* Trade Status */}
    <div className="mb-4 bg-white p-4 shadow rounded-lg">
        <h3 className="text-md font-semibold">Statut de l'Échange</h3>
        <p className="text-gray-700">{trade?.statusCode}</p>
    </div>

    {/* Action Buttons */}
    <div className="flex gap-4 mt-4">
        {trade?.receiver.id === user?.trainerId && trade.statusCode === 'PROPOSITION' && (
            <>
                <button onClick={handleAcceptTrade} className="px-4 py-2 text-white bg-green-500 rounded hover:bg-green-600">Accepter l'échange</button>
                <button onClick={handleDeclineTrade} className="px-4 py-2 text-white bg-red-500 rounded hover:bg-red-600">Refuser l'échange</button>
            </>
        )}
        <button onClick={handleBackClick} className="px-4 py-2 text-gray-700 bg-gray-200 rounded hover:bg-gray-300 flex items-center gap-2">
            <FaArrowLeft />
            Retour
        </button>
    </div>
</div>

  );
}

export default TradeDetailPage;
