// pages/EditPokemonPage.tsx
import { useState, useEffect } from 'react';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { Pokemon } from '../Types';
import { FiSave, FiArrowLeft } from 'react-icons/fi';

function EditPokemonPage() {
  const { pokemonId } = useParams<{ pokemonId: string }>();
  const [pokemon, setPokemon] = useState<Pokemon | null>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');
  const { user,logout } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchPokemonDetails = async () => {
      setIsLoading(true);
      try {
        const response = await fetch(`http://localhost:8000/pokemons/${pokemonId}`, {
          headers: {
            'Authorization': `Bearer ${user?.accessToken}`,
          },
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
      
        const data = await response.json();
        setPokemon(data);
      } catch (error) {
        if (error instanceof Error && error.message === 'Network response was not ok') {
          logout();
          navigate('/login');
        } else {
          setError('Failed to fetch pokemon details');
          console.error("Could not fetch pokemon:", error);
        }
      } finally {
        setIsLoading(false);
      }
    };

    fetchPokemonDetails();
  }, [pokemonId, user?.accessToken, logout, navigate]);

  const handleInputChange = (
    event: React.ChangeEvent<HTMLInputElement> | React.ChangeEvent<HTMLSelectElement>
  ) => {
    const target = event.target as HTMLInputElement; 
    const value = target.type === 'checkbox' ? target.checked : target.value;
    setPokemon((prev) => {
      if (prev === null) {
        return null;
      }
      return {
        ...prev,
        [target.name]: value,
      };
    });
  };
  
  const handleSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (!pokemon) return;

    try {
      const response = await fetch(`http://localhost:8000/pokemons/${pokemonId}`, {
        method: 'PATCH',
        headers: {
          'Authorization': `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(pokemon),
      });

      console.log('Fetch Response:', response); // Debugging line


      if (!response.ok) {
        throw new Error('Network response was not ok');
      }    

      navigate(`/pokemons/${pokemonId}`);
    } catch (error) {
      if (error instanceof Error) {
        console.error("Error in handleSubmit:", error); // Updated for clearer error logging

          if (error.message === 'Network response was not ok') {
              // Specific handling for network response errors
              logout();
              navigate('/login');
          } else {
              // Generic error message with details
              setError('Failed to update pokemon. Error: ' + error.message);
          }
      } else {
          // Handle cases where error is not an Error object
          console.error("An unknown error occurred:", error);
          setError('Failed to update pokemon due to an unknown error');
      }
  }
  
  };

  if (!user) return <Navigate to="/login" />;
  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>Error: {error}</div>;
  if (!pokemon) return <div>No Pokémon found</div>;

  return (
    <div className="container mx-auto p-4">
    <h1 className="text-2xl font-bold text-center mb-6">Edit Pokémon</h1>
    <div className="max-w-md mx-auto bg-white shadow-md rounded-lg p-6">
      <form onSubmit={handleSubmit} className="space-y-4">
        <div>
          <label htmlFor="pokemonSpecies" className="block text-sm font-medium text-gray-700">Pokémon Species</label>
          <input
            id="pokemonSpecies"
            type="text"
            name="species"
            value={pokemon.species}
            onChange={handleInputChange}
            className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
          />
        </div>

        <div>
          <label htmlFor="pokemonName" className="block text-sm font-medium text-gray-700">Name</label>
          <input
            id="pokemonName"
            type="text"
            name="name"
            value={pokemon.name}
            onChange={handleInputChange}
            className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
          />
        </div>

        <div>
          <label htmlFor="level" className="block text-sm font-medium text-gray-700">Level</label>
          <input
            id='level'
            type="number"
            name="level"
            value={pokemon.level}
            onChange={handleInputChange}
            min="1"
            className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
          />
        </div>

        <div>
          <label htmlFor='gender' className="block text-sm font-medium text-gray-700">Gender</label>
          <select
            id='gender'
            name="genderTypeCode"
            value={pokemon.genderTypeCode}
            onChange={handleInputChange}
            className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
          >
            <option value="MALE">Male</option>
            <option value="FEMALE">Female</option>
            <option value="NOT_DEFINED">Not Defined</option>
          </select>
        </div>

        <div className="grid grid-cols-2 gap-4">
          <div>
            <label htmlFor='size' className="block text-sm font-medium text-gray-700">Size (cm)</label>
            <input
              id='size'
              type="number"
              name="size"
              value={pokemon.size}
              onChange={handleInputChange}
              className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
            />
          </div>

          <div>
            <label htmlFor='weight' className="block text-sm font-medium text-gray-700">Weight (kg)</label>
            <input
              id='weight'
              type="number"
              name="weight"
              value={pokemon.weight}
              onChange={handleInputChange}
              className="mt-1 block w-full border border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
            />
          </div>
        </div>

        <div className="flex items-center">
          <input
            id='shiny'
            type="checkbox"
            name="isShiny"
            checked={pokemon.isShiny}
            onChange={handleInputChange}
            className="rounded text-indigo-600 focus:ring-indigo-500"
          />
          <label htmlFor='shiny' className="ml-2 text-sm font-medium text-gray-700">Shiny</label>
        </div>

        <div className="flex justify-between items-center">
          <button
            type="submit"
            className="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
          >
            <FiSave className="mr-2" />
            Save Changes
          </button>
          <button
            onClick={() => navigate(-1)}
            className="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-gray-600 bg-gray-100 hover:bg-gray-200 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-gray-500"
          >
            <FiArrowLeft className="mr-2" />
            Cancel
          </button>
        </div>
      </form>
    </div>
  </div>

  );
}

export default EditPokemonPage;
