// pages/SubscribePage.tsx

import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

function SubscribePage() {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [login, setLogin] = useState('');
  const [birthDate, setBirthDate] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const { logout } = useAuth();
  const navigate = useNavigate();

  const handleSubscribe = async () => {
    try {
      const response = await fetch('http://localhost:8000/subscribe', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ firstName, lastName, login, birthDate, password }),
      });

      if (response.ok) {
        const data = await response.json();
        if (data && data.accessToken) {
          localStorage.setItem('accessToken', data.accessToken);
          localStorage.setItem('trainerId', data.trainerId.toString());

          navigate('/'); // Redirecting to the home page
        } else {
          setError('Unknown error occurred.');
        }
      } else if (response.status === 409) {
        setError('The login is already in use.');
      } 
      else if (response.status === 401) {
        logout();
        return;
      }
      else {
        setError('An error occurred while trying to subscribe.');
      }
    } catch (err) {
      setError('An error occurred while trying to subscribe.');
    }
  };

  return (
    <div className="flex items-center justify-center h-screen bg-gray-100">
      <div className="p-6 bg-white rounded-lg shadow-md w-96">
        <h1 className="text-2xl font-bold text-center mb-4">S'inscrire</h1>
        <input
          type="text"
          placeholder="Prénom"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          className="w-full p-3 mb-4 border rounded border-gray-300"
        />
        <input
          type="text"
          placeholder="Nom de famille"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          className="w-full p-3 mb-4 border rounded border-gray-300"
        />
        <input
          type="email"
          placeholder="Nom d’utilisateur"
          value={login}
          onChange={(e) => setLogin(e.target.value)}
          className="w-full p-3 mb-4 border rounded border-gray-300"
        />
        <input
          type="date"
          placeholder="Date de naissance"
          value={birthDate}
          onChange={(e) => setBirthDate(e.target.value)}
          className="w-full p-3 mb-4 border rounded border-gray-300"
        />
        <input
          type="password"
          placeholder="Mot de Passe"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className="w-full p-3 mb-4 border rounded border-gray-300"
        />
        <button
          onClick={handleSubscribe}
          className="w-full bg-blue-500 text-white p-3 rounded hover:bg-blue-600"
        >
          S'inscrire
        </button>
        {error && <p className="text-red-500 text-center mt-2">{error}</p>}
      </div>
    </div>
  );
}

export default SubscribePage;
