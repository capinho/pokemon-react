// pages/SearchPokemonsPage.tsx
import { useState, useEffect } from 'react';
import { useAuth } from '../context/AuthContext';
import { Pokemon } from '../Types';
import { useNavigate } from 'react-router-dom';

function SearchPokemonsPage() {

  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [species, setSpecies] = useState('');
  const [name, setName] = useState('');
  const [levelMin, setLevelMin] = useState('');
  const [levelMax, setLevelMax] = useState('');
  const [sizeMin, setSizeMin] = useState('');
  const [sizeMax, setSizeMax] = useState('');
  const [weightMin, setWeightMin] = useState('');
  const [weightMax, setWeightMax] = useState('');
  const [isShiny, setIsShiny] = useState('');
  const [page, setPage] = useState(0);
  const [noResults, setNoResults] = useState(false);

  const pageSize = 20;
  const [error, setError] = useState('');
  const { user,logout } = useAuth();
  const navigate = useNavigate();

  const navigateToTrade = (event: React.MouseEvent<HTMLButtonElement>, pokemonId: number, receiverId: number) => {
    event.stopPropagation();
    navigate(`/create-trade?pokemonId=${pokemonId}&receiverId=${receiverId}`);
  };
        
  const navigateToPokemonDetail = (pokemonId: number) => {
    navigate(`/pokemons/${pokemonId}`);
  };

  
  useEffect(() => {
    const fetchPokemons = async () => {
      
        try {
          // Create an object with only the non-empty parameters
          const params = {
            ...(species && { species }),
            ...(name && { name }),
            ...(levelMin && { levelMin }),
            ...(levelMax && { levelMax }),
            ...(sizeMin && { sizeMin }),
            ...(sizeMax && { sizeMax }),
            ...(weightMin && { weightMin }),
            ...(weightMax && { weightMax }),
            ...(isShiny && { isShiny }),
            page: page.toString(),
            pageSize: pageSize.toString(),
          };
          console.log(params);
          // Create a query string from the params object
          const queryString = new URLSearchParams(params);
          
          const response = await fetch(`http://localhost:8000/pokemons?${queryString.toString()}`, {
            headers: {
              'Authorization': `Bearer ${user?.accessToken}`,
            },
          });
          if (!response.ok) {
            const errorText = await response.text();
            throw new Error(`HTTP error: ${response.status} - ${errorText}`);
          }
                    
        
          const data = await response.json();
          if (data.length === 0) {
            setNoResults(true);
          } else {
            setPokemons(data);
          }
    
        } catch (error) {
          if (error instanceof Error) {
            console.error('Error fetching pokemons:', error.message);
            if (error.message.includes('401')) {
              logout();
              navigate('/login');
            } else {
              setError('Failed to fetch pokemons');
            }
          }        
        }
      };
      
    fetchPokemons();
  }, [user, species, name, levelMin, levelMax, sizeMin, sizeMax, weightMin, weightMax, isShiny, page, logout, navigate]);

  const handleSpeciesChange = (e: React.ChangeEvent<HTMLInputElement>) => setSpecies(e.target.value);
  const handleNameChange = (e: React.ChangeEvent<HTMLInputElement>) => setName(e.target.value);
  const handleLevelMinChange = (e: React.ChangeEvent<HTMLInputElement>) => setLevelMin(e.target.value);
  const handleLevelMaxChange = (e: React.ChangeEvent<HTMLInputElement>) => setLevelMax(e.target.value);
  const handleSizeMinChange = (e: React.ChangeEvent<HTMLInputElement>) => setSizeMin(e.target.value);
  const handleSizeMaxChange = (e: React.ChangeEvent<HTMLInputElement>) => setSizeMax(e.target.value);
  const handleWeightMinChange = (e: React.ChangeEvent<HTMLInputElement>) => setWeightMin(e.target.value);
  const handleWeightMaxChange = (e: React.ChangeEvent<HTMLInputElement>) => setWeightMax(e.target.value);
  const handleIsShinyChange = (e: React.ChangeEvent<HTMLInputElement>) => setIsShiny(e.target.checked ? 'true' : '');

  const goToNextPage = () => setPage((prevPage) => prevPage + 1);
  const goToPrevPage = () => setPage((prevPage) => Math.max(prevPage - 1, 0));

  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-6">Search Pokémons</h1>
      {error && <p className="text-red-500">{error}</p>}
      
      {/* Input fields for filters */}
      <div className="grid grid-cols-2 gap-4 mb-6">
        <input type="text" value={species} onChange={handleSpeciesChange} placeholder="Species" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <input type="text" value={name} onChange={handleNameChange} placeholder="Name" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <input type="number" value={levelMin} onChange={handleLevelMinChange} placeholder="Min Level" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <input type="number" value={levelMax} onChange={handleLevelMaxChange} placeholder="Max Level" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <input type="number" value={sizeMin} onChange={handleSizeMinChange} placeholder="Min Size" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <input type="number" value={sizeMax} onChange={handleSizeMaxChange} placeholder="Max Size" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <input type="number" value={weightMin} onChange={handleWeightMinChange} placeholder="Min Weight" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <input type="number" value={weightMax} onChange={handleWeightMaxChange} placeholder="Max Weight" className="py-2 px-4 border border-gray-300 rounded-md shadow-sm" />
        <div className="flex items-center">
          <label className="mr-2">Shiny:</label>
          <input type="checkbox" checked={isShiny === 'true'} onChange={handleIsShinyChange} className="rounded text-blue-600" />
        </div>
      </div>

      {/* Pokemon table */}
      {
        pokemons.length > 0 ? (
          <table className="min-w-full table-auto bg-white rounded-lg shadow-md overflow-hidden">
            <thead className="bg-gray-100">
              <tr>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Species</th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Name</th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Level</th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Shiny</th>
                <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Actions</th>
              </tr>
            </thead>
            <tbody className="bg-white">
              {pokemons.map((pokemon) => (
                <tr key={pokemon.id} onClick={() => navigateToPokemonDetail(pokemon.id)} className="border-b hover:bg-gray-50 cursor-pointer">
                  <td className="px-6 py-4 whitespace-nowrap">{pokemon.species}</td>
                  <td className="px-6 py-4 whitespace-nowrap">{pokemon.name}</td>
                  <td className="px-6 py-4 whitespace-nowrap">{pokemon.level}</td>
                  <td className="px-6 py-4 whitespace-nowrap">{pokemon.isShiny ? 'Yes' : 'No'}</td>
                  <td className="px-6 py-4 whitespace-nowrap">
                    {pokemon.trainerId !== user?.trainerId && (
                      <button 
                        onClick={(event) => navigateToTrade(event, pokemon.id, pokemon.trainerId)}
                        className="text-indigo-600 hover:text-indigo-900"
                      >
                        Trade
                      </button>
                    )}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        ) : noResults ? (
          <p className="text-center">Aucun résultat.</p>
        ) : (
          <p className="text-center">Loading...</p>
        )
      }

      {/* Pagination controls */}
      <div className="flex justify-between items-center mt-6">
        <button 
          onClick={goToPrevPage}
          className="px-4 py-2 text-sm font-medium text-white bg-blue-500 rounded-md hover:bg-blue-700 disabled:opacity-50 disabled:cursor-not-allowed"
          disabled={page === 0}
        >
          Previous 20 Pokémons
        </button>
        <button 
          onClick={goToNextPage}
          className="px-4 py-2 text-sm font-medium text-white bg-blue-500 rounded-md hover:bg-blue-700"
          disabled={pokemons.length < pageSize}
        >
          Next 20 Pokémons
        </button>
      </div>
    </div>

  );
}

export default SearchPokemonsPage;
