//pages/BoxDetail.tsx
import { useEffect, useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { Box, Pokemon } from '../Types';
import { useAuth } from '../context/AuthContext';
import { FaArrowCircleLeft, FaPlus } from 'react-icons/fa';

function BoxDetail() {
  const { boxId } = useParams<{ boxId: string }>();
  const [box, setBox] = useState<Box | null>(null);
  const [pokemons, setPokemons] = useState<Pokemon[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');
  const { user,logout } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchBoxDetail = async () => {
      setIsLoading(true);
      try {
        const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/boxes/${boxId}`, {
          headers: {
            'Authorization': `Bearer ${user?.accessToken}`
          }
        });
  
  
        if (!response.ok) {
          throw new Error('Network response was not ok');
        }
  
        const data = await response.json();
        setBox(data.box);
        setPokemons(data.pokemons);
      } catch (error) {
        if (error instanceof Error && error.message === 'Network response was not ok') {
          logout();
          navigate('/login');
        } else {
          setError("Erreur lors de la récupération du détail de la boîte.");
          console.error("Erreur lors de la récupération du détail de la boîte:", error);
        }
      } finally {
        setIsLoading(false);
      }
    };
  
    fetchBoxDetail();
  }, [boxId, user, navigate,logout]);
  

  if (isLoading) {
    return <div className="text-center my-5">Loading...</div>;
  }

  if (error) {
    return <div className="text-red-500 text-center my-5">{error}</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <button onClick={() => navigate(-1)} className="text-blue-600 hover:text-blue-800 flex items-center mb-5">
        <FaArrowCircleLeft className="mr-2" /> Retour
      </button>

      <div className="bg-white shadow rounded-lg p-6">
        <h1 className="text-2xl font-bold mb-4">Détail de la boîte: {box?.name}</h1>
        {pokemons.length > 0 ? (
          <ul className="space-y-4">
            {pokemons.map(pokemon => (
              <li key={pokemon.id} className="border border-gray-300 p-3 rounded-md hover:bg-gray-100 cursor-pointer" onClick={() => navigate(`/pokemons/${pokemon.id}`)}>

                Espèce: {pokemon.species} <br />
                Nom: {pokemon.name} <br />
                Niveau: {pokemon.level} <br />
                Genre: {pokemon.genderTypeCode} <br />
                Chromatique: {pokemon.isShiny ? "Oui" : "Non"}
                <button
                  className="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded inline-flex items-center"
                  onClick={(e) => {
                    e.stopPropagation(); // Arrête la propagation de l'événement
                    navigate(`/move-pokemon/${pokemon.id}`);
                  }}
                >
                  Déplacer
                </button>

              </li>
            ))}
          </ul>
        ) : (
          <p className="text-gray-700">La boîte est vide.</p>
        )}
        <div className="mt-4">
          <button onClick={() => navigate('/add-pokemon')} className="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded inline-flex items-center">
            <FaPlus className="mr-2" /> Ajout d'un Pokémon
          </button>
          <button onClick={() => navigate(`/rename-box/${boxId}`)} className="bg-green-600 hover:bg-green-700 text-white font-bold py-2 px-4 rounded inline-flex items-center ml-2">
              Renommer la boîte
          </button>
          <button onClick={() => navigate(`/delete-box/${boxId}`)} className="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 rounded inline-flex items-center ml-2">
            Supprimer la boîte
          </button>
        </div>
      </div>
    </div>
  );

}

export default BoxDetail;
