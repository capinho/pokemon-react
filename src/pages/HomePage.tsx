//pages/HomePage.tsx
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

const HomePage = () => {
  const navigate = useNavigate();
  const { user } = useAuth();

  useEffect(() => {
    if (user) {
      navigate('/boxes');
    } else {
      navigate('/login');
    }
  }, [user, navigate]);

  return null; // Rien à afficher
};

export default HomePage;
