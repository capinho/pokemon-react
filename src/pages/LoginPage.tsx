//pages/LoginPage.tsx
import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

function LoginPage() {
  const [loginInput, setLogin] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const { user, login: loginUser } = useAuth();
  const navigate = useNavigate();

  useEffect(() => {
    if (user) {
      navigate('/boxes');
    }
  }, [user, navigate]);

  const handleLogin = async () => {
    try {
      const response = await fetch('http://localhost:8000/login', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ login: loginInput, password }),
      });

      if (response.ok) {
        const data = await response.json();
        if (data && data.accessToken) {
          const expiresIn = data.expiresIn; 

          loginUser(data.accessToken, data.trainerId, expiresIn);
          navigate('/boxes'); 
        } else {
          setError('Could not log in with the provided credentials.');
        }
      } else {
        setError('Invalid username or password.');
      }
    } catch (err) {
      setError('There was an error processing your login.');
    }
  };

  return (
    <div className="flex items-center justify-center h-screen bg-gray-100">
      <div className="p-6 bg-white rounded-lg shadow-md w-96">
        <h1 className="text-2xl font-bold text-center mb-4">Login</h1>
        <input
          type="email"
          placeholder="Username"
          value={loginInput}
          onChange={(e) => setLogin(e.target.value)}
          className="w-full p-3 mb-4 border rounded border-gray-300"
        />
        <input
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className="w-full p-3 mb-4 border rounded border-gray-300"
        />
        <button
          onClick={handleLogin}
          className="w-full bg-blue-500 text-white p-3 rounded hover:bg-blue-600"
        >
          Login
        </button>
        {error && <p className="text-red-500 text-center mt-2">{error}</p>}
      </div>
    </div>
  );
}

export default LoginPage;