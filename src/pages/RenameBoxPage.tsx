// RenameBoxPage.tsx
import { useState } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

function RenameBoxPage() {
    const { boxId } = useParams<{ boxId: string }>();
  const [newName, setNewName] = useState('');
  const [error, setError] = useState('');
  const { user, logout } = useAuth();
  const navigate = useNavigate();

  const handleRename = async () => {
    if (!newName.trim()) {
      setError('Veuillez entrer un nom valide.');
      return;
    }

    try {
      const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/boxes/${boxId}`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer ${user?.accessToken}`,
        },
        body: JSON.stringify({ name: newName }),
      });

      if (!response.ok) {
        throw new Error('Échec de la mise à jour du nom de la boîte');
      }

      // Redirection vers la page de détails de la boîte
      navigate(`/boxes/${boxId}`);
    } catch (error) {
      if (error instanceof Error) {
        setError(error.message);
        if (error.message.includes('401')) {
          logout();
          navigate('/login');
        }
      }
    }
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Renommer la Boîte</h1>
      {error && <p className="text-red-500">{error}</p>}
      <input
        type="text"
        value={newName}
        onChange={(e) => setNewName(e.target.value)}
        placeholder="Nouveau nom de la boîte"
        className="border border-gray-300 p-2 rounded"
      />
      <button
        onClick={handleRename}
        className="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded ml-2"
      >
        Renommer
      </button>
    </div>
  );
}

export default RenameBoxPage;
