// pages/DeleteBoxPage.tsx
import { useParams, useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

function DeleteBoxPage() {
  const { boxId } = useParams<{ boxId: string }>();
  const { user, logout } = useAuth();
  const navigate = useNavigate();

  const handleDelete = async () => {
    try {
      const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/boxes/${boxId}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${user?.accessToken}`,
        },
      });

      if (!response.ok) {
        throw new Error('Échec de la suppression de la boîte');
      }

      // Redirection après suppression réussie
      navigate('/boxes');
    } catch (error) {
      if (error instanceof Error) {
        console.error('Delete error:', error);
        if (error.message.includes('401')) {
          logout();
          navigate('/login');
        }
      }
    }
  };

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Suppression de la Boîte</h1>
      <p>Êtes-vous sûr de vouloir supprimer cette boîte ? Cette action est irréversible.</p>
      <button onClick={handleDelete} className="bg-red-600 hover:bg-red-700 text-white font-bold py-2 px-4 rounded">
        Supprimer
      </button>
      <button onClick={() => navigate(-1)} className="bg-gray-600 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded ml-2">
        Annuler
      </button>
    </div>
  );
}

export default DeleteBoxPage;
