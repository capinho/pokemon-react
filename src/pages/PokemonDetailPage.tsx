// pages/PokemonDetailPage.tsx
import { useState, useEffect } from 'react';
import { useParams, useNavigate, Navigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { Pokemon } from '../Types'; 

function PokemonDetailPage() {
  const { pokemonId } = useParams<{ pokemonId: string }>();
  const [pokemon, setPokemon] = useState<Pokemon | null>(null);
  const { user, logout } = useAuth();
  const navigate = useNavigate();
  
  useEffect(() => {
    if (user && new Date().getTime() > user.expiresAt) {
      logout();
      navigate('/login');
    } else {

    const fetchPokemon = async () => {
      try {
        const response = await fetch(`http://localhost:8000/pokemons/${pokemonId}`, {
          headers: {
            'Authorization': `Bearer ${user?.accessToken}`,
          },
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        const data = await response.json();
        setPokemon(data);
      } catch (error) {
        if (error instanceof Error && error.message === 'Network response was not ok') {
          logout();
          navigate('/login');
        } else {
          console.error("Could not fetch pokemon:", error);
        }
      }
    };

    fetchPokemon();
  }
  }, [pokemonId, user, logout, navigate]);

  if (!user) {
    return <Navigate to="/login" />;
  }

  if (!pokemon) {
    return <div>Loading...</div>;
  }

  const handleEditClick = () => {
    navigate(`/pokemons/${pokemonId}/edit`);
  };

  const handleBackClick = () => {
    navigate(`/boxes/${pokemon.boxId}`);
  };

  const handleDeleteClick = async () => {
    // Make a DELETE request to delete the pokemon
    const boxId = pokemon.boxId;

    try {
      const response = await fetch(`http://localhost:8000/pokemons/${pokemonId}`, {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer ${user.accessToken}`,
        },
      });

      if (!response.ok) {
        throw new Error('Network response was not ok');
      }

      // Redirect user to the boxes page after deletion
      navigate(`/boxes/${boxId}`);
    } catch (error) {
      console.error("Could not delete pokemon:", error);
    }
  };

  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-4">Pokemon Detail</h1>
      <div className="bg-white p-6 rounded-lg shadow-md">
        <p className="mb-2">Species: {pokemon.species}</p>
        <p className="mb-2">Name: {pokemon.name}</p>
        <p className="mb-2">Level: {pokemon.level}</p>
        <p className="mb-2">Gender: {pokemon.genderTypeCode}</p>
        <p className="mb-2">Size: {pokemon.size} cm</p>
        <p className="mb-2">Weight: {pokemon.weight} kg</p>
        <p className="mb-4">Shiny: {pokemon.isShiny ? 'Yes' : 'No'}</p>
        {user.trainerId === pokemon.trainerId && (
          <div className="flex gap-4 mb-4">
            <button onClick={handleEditClick} className="px-4 py-2 bg-blue-500 text-white rounded hover:bg-blue-600">Edit</button>
            <button onClick={handleDeleteClick} className="px-4 py-2 bg-red-500 text-white rounded hover:bg-red-600">Delete</button>
          </div>
        )}
        <button onClick={handleBackClick} className="px-4 py-2 bg-gray-200 text-gray-700 rounded hover:bg-gray-300">Back</button>
      </div>
    </div>

  );
}

export default PokemonDetailPage;
