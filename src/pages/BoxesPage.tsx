//pages/BoxesPage.tsx
import { useState, useEffect } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { Box } from '../Types';
import { useAuth } from '../context/AuthContext';
import { FaBoxOpen, FaPlusSquare } from 'react-icons/fa';

function BoxesPage() {
    const [boxes, setBoxes] = useState<Box[]>([]);
    const [isLoading, setIsLoading] = useState(true);
    const [error, setError] = useState('');
    const { user, logout } = useAuth();
    const navigate = useNavigate();

    useEffect(() => {
        const fetchBoxes = async () => {
            setIsLoading(true);
            try {
                const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/boxes`, {
                    headers: {
                        'Authorization': `Bearer ${user?.accessToken}`
                    }
                });

                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }

                const data = await response.json();
                setBoxes(data);
            } catch (err) {
                if (err instanceof Error && err.message === 'Network response was not ok') {
                    logout();
                    navigate('/login');
                } else {
                    setError('Une erreur s’est produite lors de la récupération des boîtes.');
                }
            } finally {
                setIsLoading(false);
            }
        };

        if (user) {
            fetchBoxes();
        }
    }, [user, navigate, logout]);

    if (isLoading) {
        return <div>Loading...</div>;
    }

    if (error) {
        return <div>{error}</div>;
    }


    return (
        <div className="container mx-auto p-4">
          <div className="flex justify-between items-center mb-4">
            <h1 className="text-3xl font-bold flex items-center">
              <FaBoxOpen className="mr-2 text-xl" /> Mes Boîtes
            </h1>
            <Link to="/create-box" className="bg-blue-600 text-white py-2 px-4 rounded flex items-center hover:bg-blue-700 transition-colors">
              <FaPlusSquare className="mr-2" /> Ajouter une nouvelle boîte
            </Link>
          </div>
    
          {isLoading ? (
            <div>Loading...</div>
          ) : error ? (
            <div>{error}</div>
          ) : (
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
              {boxes.length > 0 ? (
                boxes.map(box => (
                  <div key={box.id} className="bg-white shadow rounded-lg p-4 hover:shadow-md transition-shadow">
                    <Link to={`/boxes/${box.id}`} className="flex items-center space-x-2">
                      <FaBoxOpen />
                      <span>{box.name}</span>
                    </Link>
                  </div>
                ))
              ) : (
                <p>Vous n'avez aucune boîte.</p>
              )}
            </div>
          )}
        </div>
      );
    
    
}

export default BoxesPage;
