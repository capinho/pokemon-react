// SearchTrainersPage.tsx
import { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { UserProfile } from '../Types';

function SearchTrainersPage() {
  const { user,logout } = useAuth();
  const navigate = useNavigate();

  const [trainers, setTrainers] = useState<UserProfile[]>([]);
  const [firstNameFilter, setFirstNameFilter] = useState('');
  const [lastNameFilter, setLastNameFilter] = useState('');
  const [loginFilter, setLoginFilter] = useState('');
  const [page, setPage] = useState(0);
  const pageSize = 20;

  useEffect(() => {
    const fetchTrainers = async () => {
      try {
        const queryParams = new URLSearchParams({
          page: page.toString(),
          pageSize: pageSize.toString(),
          ...(firstNameFilter && { firstName: firstNameFilter }),
          ...(lastNameFilter && { lastName: lastNameFilter }),
          ...(loginFilter && { login: loginFilter }),
        });

        const response = await fetch(`http://localhost:8000/trainers?${queryParams}`, {
          headers: { 'Authorization': `Bearer ${user?.accessToken}` },
        });

        if (!response.ok) {
          throw new Error('Failed to fetch trainers');
        }

        const data = await response.json();
        setTrainers(data);
      } catch (error) {
        if (error instanceof Error && error.message === 'Failed to fetch trainers') {
          logout();
          navigate('/login');
        } else {
          console.error('Error:', error);
        }
      }
    };

    fetchTrainers();
  }, [firstNameFilter, lastNameFilter, loginFilter, page, pageSize, user?.accessToken, logout, navigate]);

  // const handleNavigateToProfile = (trainerProfileId: number) => {
  //   const profileId = user?.trainerId === trainerProfileId ? "" : `/${trainerProfileId}`;
  //   navigate(`/profile${profileId}`);
  // };

  const handleNavigateToCreateTrade = (receiverId: number) => {
    navigate(`/create-trade?receiverId=${receiverId}`);
  };
  
  const handleRowClick = (trainerProfileId: number) => {
    navigate(`/profile/${trainerProfileId}`);
  };



  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-6">Search Trainers</h1>

      <div className="flex gap-4 mb-6">
        <input
          type="text"
          value={firstNameFilter}
          onChange={(e) => setFirstNameFilter(e.target.value)}
          placeholder="Prénom"
          className="py-2 px-4 border border-gray-300 rounded-md shadow-sm"
        />
        <input
          type="text"
          value={lastNameFilter}
          onChange={(e) => setLastNameFilter(e.target.value)}
          placeholder="Nom de famille"
          className="py-2 px-4 border border-gray-300 rounded-md shadow-sm"
        />
        <input
          type="text"
          value={loginFilter}
          onChange={(e) => setLoginFilter(e.target.value)}
          placeholder="Login"
          className="py-2 px-4 border border-gray-300 rounded-md shadow-sm"
        />
      </div>

      <table className="min-w-full table-auto bg-white rounded-lg shadow-md overflow-hidden">
        <thead className="bg-gray-100">
          <tr>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Prénom</th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Nom de famille</th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Login</th>
            <th className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider">Actions</th>
          </tr>
        </thead>
        <tbody className="bg-white">
          {trainers.map((trainer) => (
            <tr key={trainer.id} onClick={() => handleRowClick(trainer.id)} className="border-b hover:bg-gray-50 cursor-pointer">
              <td className="px-6 py-4 whitespace-nowrap">{trainer.firstName}</td>
              <td className="px-6 py-4 whitespace-nowrap">{trainer.lastName}</td>
              <td className="px-6 py-4 whitespace-nowrap">{trainer.login}</td>
              <td className="px-6 py-4 whitespace-nowrap">
                {trainer.id !== user?.trainerId && (
                  <button 
                    onClick={(e) => {
                      e.stopPropagation();
                      handleNavigateToCreateTrade(trainer.id);
                    }}
                    className="text-indigo-600 hover:text-indigo-900"
                  >
                    Echanger
                  </button>
                )}
              </td>
            </tr>
          ))}
        </tbody>
      </table>

      <div className="flex justify-between items-center mt-6">
        <button 
          onClick={() => setPage((prev) => Math.max(prev - 1, 0))}
          className="px-4 py-2 text-sm font-medium text-white bg-blue-500 rounded-md hover:bg-blue-700 disabled:opacity-50 disabled:cursor-not-allowed"
        >
          Précédent
        </button>
        <button 
          onClick={() => setPage((prev) => prev + 1)}
          className="px-4 py-2 text-sm font-medium text-white bg-blue-500 rounded-md hover:bg-blue-700"
        >
          Suivant
        </button>
      </div>
    </div>

  );
}

export default SearchTrainersPage;
