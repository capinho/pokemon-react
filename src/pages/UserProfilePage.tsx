//pages/UserProfilePage.tsx
import { useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { UserProfile } from '../Types';
import { useAuth } from '../context/AuthContext';

function UserProfilePage() {

  const { user,logout } = useAuth();
  const navigate = useNavigate();
  const [userProfile, setUserProfile] = useState<UserProfile | null>(null);
  const [error, setError] = useState<string | null>(null);
  const { trainerId } = useParams();

  useEffect(() => {
    const fetchUserProfile = async () => {
      const id = trainerId ? trainerId : user?.trainerId;
      if (!id) {
        setError('ID du dresseur non disponible');
        return;
      }

      try {
        const response = await fetch(`http://localhost:8000/trainers/${id}`, {
          headers: { 'Authorization': `Bearer ${user?.accessToken}` },
        });

        if (!response.ok) {
          throw new Error('Réponse réseau non valide');
        }

        const data = await response.json();
        setUserProfile(data);
      } catch (error) {
        if (error instanceof Error && error.message === 'Réponse réseau non valide') {
          logout()
          navigate('/login');
        } else {
          console.error('Erreur lors de la récupération du profil utilisateur:', error);
          setError('Erreur lors de la récupération du profil utilisateur');
        }
      }
    };

    fetchUserProfile();
  }, [user?.accessToken, user?.trainerId, trainerId, navigate, logout]);

  if (error) {
    return <div>Erreur: {error}</div>;
  }

  if (!userProfile) {
    return <div>Chargement du profil en cours...</div>;
  }

  const handleBackClick = () => {
    navigate(-1); 
  };
  const handleEditClick = () => {
    navigate('/user-edit');
  };

  const isCurrentUserProfile = user?.trainerId === userProfile.id;

  return (
    <div className="container mx-auto px-4 py-8">
      <h1 className="text-2xl font-bold mb-6">Profil Utilisateur</h1>
      {error ? (
        <div className="text-red-500">Erreur: {error}</div>
      ) : userProfile ? (
        <div className="bg-white p-6 rounded-lg shadow-md">
          <p className="mb-4"><strong>Prénom:</strong> {userProfile.firstName}</p>
          <p className="mb-4"><strong>Nom:</strong> {userProfile.lastName}</p>
          <p className="mb-4"><strong>Login:</strong> {userProfile.login}</p>
          <p className="mb-4"><strong>Date de naissance:</strong> {userProfile.birthDate}</p>

          <div className="flex gap-4">
            {isCurrentUserProfile && (

            <button onClick={handleEditClick} className="px-4 py-2 text-sm font-medium text-white bg-blue-500 rounded-md hover:bg-blue-700">Modifier mes informations</button>
            )}
            <button onClick={handleBackClick} className="px-4 py-2 text-sm font-medium text-gray-700 bg-gray-200 rounded-md hover:bg-gray-300">Retour</button>
          </div>
        </div>
      ) : (
        <div className="text-center">Chargement du profil en cours...</div>
      )}
    </div>

  );
}

export default UserProfilePage;
