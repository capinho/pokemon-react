// pages/CreatePokemonPage.tsx
import React, { useState } from 'react';
import { useAuth } from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';

function CreatePokemonPage() {
    const [pokemonName, setPokemonName] = useState('');
    const [pokemonSpecies, setPokemonSpecies] = useState('');
    const [pokemonLevel, setPokemonLevel] = useState(1);
    const [genderTypeCode, setGenderTypeCode] = useState('NOT_DEFINED');
    const [isShiny, setIsShiny] = useState(false);
    const [pokemonSize, setPokemonSize] = useState(0);
    const [pokemonWeight, setPokemonWeight] = useState(0);
    const [error, setError] = useState('');
    const [isSubmitting, setIsSubmitting] = useState(false);
    const { user,logout } = useAuth();
    const navigate = useNavigate();

    const handleSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();

        if (!pokemonSpecies) {
            setError('Species is required.');
            return;
        }

        setIsSubmitting(true);
        try {
            const response = await fetch('http://localhost:8000/pokemons', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${user?.accessToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: pokemonName,
                    species: pokemonSpecies,
                    level: pokemonLevel,
                    genderTypeCode: genderTypeCode,
                    isShiny: isShiny,
                    size: pokemonSize,
                    weight: pokemonWeight,
                }),
            });

            if (!response.ok) {
                throw new Error('Network response was not ok');
            }
            if (response.status === 401) {
                logout();
                navigate('/login');
            }
            
            const data = await response.json();
            navigate(`/pokemons/${data.id}`);
        } catch (error) {
            setError('Failed to create Pokémon. Please try again later.');
        } finally {
            setIsSubmitting(false);
        }
    };

    return (
        <div className="flex items-center justify-center h-screen bg-gray-100">
            <div className="p-6 bg-white rounded-lg shadow-md w-full max-w-md">
                <h1 className="text-2xl font-bold text-center mb-4">Add a Pokémon</h1>
                {error && <p className="text-red-500 text-center">{error}</p>}
                <form onSubmit={handleSubmit} className="space-y-4">
                    <div>
                        <label className="block text-sm font-medium text-gray-700">
                            Pokémon Name:
                            <input
                                type="text"
                                value={pokemonName}
                                onChange={(e) => setPokemonName(e.target.value)}
                                className="mt-1 block w-full p-2 border border-gray-300 rounded-md"
                            />
                        </label>
                    </div>
                    <div>
                        <label className="block text-sm font-medium text-gray-700">
                            Pokémon Species:
                            <input
                                type="text"
                                value={pokemonSpecies}
                                onChange={(e) => setPokemonSpecies(e.target.value)}
                                className="mt-1 block w-full p-2 border border-gray-300 rounded-md"
                            />
                        </label>
                    </div>
                    <div>
                        <label className="block text-sm font-medium text-gray-700">
                            Level:
                            <input
                                type="number"
                                value={pokemonLevel}
                                onChange={(e) => setPokemonLevel(Number(e.target.value))}
                                className="mt-1 block w-full p-2 border border-gray-300 rounded-md"
                            />
                        </label>
                    </div>
                    <div>
                        <label className="block text-sm font-medium text-gray-700">
                            Gender:
                            <select
                                value={genderTypeCode}
                                onChange={(e) => setGenderTypeCode(e.target.value)}
                                className="mt-1 block w-full p-2 border border-gray-300 rounded-md bg-white"
                            >
                                <option value="MALE">Male</option>
                                <option value="FEMALE">Female</option>
                                <option value="NOT_DEFINED">Not Defined</option>
                            </select>
                        </label>
                    </div>
                    <div className="flex items-center">
                        <label htmlFor="isShiny" className="block text-sm font-medium text-gray-700 mr-2">
                            Is Shiny?
                        </label>
                        <input
                            id="isShiny"
                            type="checkbox"
                            checked={isShiny}
                            onChange={(e) => setIsShiny(e.target.checked)}
                            className="rounded text-blue-600"
                        />
                    </div>
                    <div>
                        <label className="block text-sm font-medium text-gray-700">
                            Size (in cm):
                            <input
                                type="number"
                                value={pokemonSize}
                                onChange={(e) => setPokemonSize(Number(e.target.value))}
                                className="mt-1 block w-full p-2 border border-gray-300 rounded-md"
                            />
                        </label>
                    </div>
                    <div>
                        <label className="block text-sm font-medium text-gray-700">
                            Weight (in kg):
                            <input
                                type="number"
                                value={pokemonWeight}
                                onChange={(e) => setPokemonWeight(Number(e.target.value))}
                                className="mt-1 block w-full p-2 border border-gray-300 rounded-md"
                            />
                        </label>
                    </div>
                    <button
                        type="submit"
                        disabled={isSubmitting}
                        className={`mt-4 w-full bg-blue-500 text-white p-3 rounded hover:bg-blue-600 ${isSubmitting ? 'opacity-50' : ''}`}
                    >
                        {isSubmitting ? 'Adding...' : 'Add Pokémon'}
                    </button>
                </form>
            </div>
        </div>

    );
}

export default CreatePokemonPage;
