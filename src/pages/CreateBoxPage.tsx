//pages/CreateBoxPage.tsx
import { useState } from 'react';
import { useAuth } from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';
import { FaPlusCircle } from 'react-icons/fa';

function CreateBoxPage() {
    const [boxName, setBoxName] = useState('');
    const [error, setError] = useState('');
    const [isSubmitting, setIsSubmitting] = useState(false);
    const { user,logout } = useAuth();
    const navigate = useNavigate();

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();

        if (!boxName.trim()) {
            setError('Please enter a name for the box.');
            return;
        }

        setIsSubmitting(true);
        setError('');
        try {
            const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/boxes`, {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${user?.accessToken}`,
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ name: boxName }),
            });

            const responseData = await response.json();

            if (response.status === 401) {
                logout();
                navigate('/login');
            }
            
            if (response.ok) {
                navigate('/boxes');
            } else {
                setError(responseData.message || 'Error creating the box.');
            }
        } catch (err) {
            setError('Error creating the box.');
        } finally {
            setIsSubmitting(false);
        }
    };


    return (
        <div className="container mx-auto p-4">
          <div className="max-w-md mx-auto bg-white shadow-lg rounded-lg p-6">
            <h1 className="text-2xl font-bold mb-4 flex items-center justify-center">
              <FaPlusCircle className="mr-2" /> Créer une nouvelle boîte
            </h1>
            <form onSubmit={handleSubmit} className="space-y-4">
              <div>
                <label htmlFor="boxName" className="block text-sm font-medium text-gray-700">
                  Nom de la boîte:
                </label>
                <div className="mt-1">
                  <input
                    id="boxName"
                    type="text"
                    value={boxName}
                    onChange={(e) => setBoxName(e.target.value)}
                    disabled={isSubmitting}
                    className="shadow-sm focus:ring-indigo-500 focus:border-indigo-500 block w-full sm:text-sm border-gray-300 rounded-md"
                    placeholder="Nom de la boîte"
                  />
                </div>
              </div>
              {error && <p className="text-red-500 text-sm">{error}</p>}
              <button
                type="submit"
                disabled={isSubmitting}
                className="w-full flex justify-center py-2 px-4 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                {isSubmitting ? 'Création...' : 'Créer'}
              </button>
            </form>
          </div>
        </div>
      );
    
}

export default CreateBoxPage;
