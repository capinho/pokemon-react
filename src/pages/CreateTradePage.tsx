// CreateTradePage.tsx
import { useState, useEffect } from 'react';
import { useAuth } from '../context/AuthContext';
import { useNavigate, useLocation } from 'react-router-dom';
import { Pokemon, UserProfile } from '../Types';

function CreateTradePage() {
    const { user, logout } = useAuth();
    const navigate = useNavigate();
    const location = useLocation();

    const [userPokemons, setUserPokemons] = useState<Pokemon[]>([]);
    const [receiverPokemons, setReceiverPokemons] = useState<Pokemon[]>([]);
    const [selectedUserPokemonIds, setSelectedUserPokemonIds] = useState<number[]>([]);
    const [selectedReceiverPokemonIds, setSelectedReceiverPokemonIds] = useState<number[]>([]);
    const [userProfile, setUserProfile] = useState<UserProfile | null>(null);
    const [receiverProfile, setReceiverProfile] = useState<UserProfile | null>(null);
    const [error, setError] = useState('');

    const queryParams = new URLSearchParams(location.search);
    const pokemonIdToTrade = queryParams.get('pokemonId');
    const receiverId = queryParams.get('receiverId');

    const isPokemonSelected = (pokemonId: number, selectedPokemonIds: number[]) => selectedPokemonIds.includes(pokemonId);

    useEffect(() => {
        const fetchUserProfile = async () => {
            try {
                const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}`, {
                    headers: { 'Authorization': `Bearer ${user?.accessToken}` }
                });
                if (!response.ok) throw new Error('Network response was not ok');
                const profileData = await response.json();
                setUserProfile(profileData);
            } catch (error) {
                if (error instanceof Error && error.message === 'Network response was not ok') {
                    logout();
                    navigate('/login');
                } else {
                    setError('Une erreur est survenue lors de la récupération du profil de l\'utilisateur.');
                    console.error('Error fetching user profile:', error);
                }
            }
        };

        const fetchReceiverProfile = async () => {
            if (!receiverId) return;
            try {
                const response = await fetch(`http://localhost:8000/trainers/${receiverId}`, {
                    headers: { 'Authorization': `Bearer ${user?.accessToken}` }
                });
                if (!response.ok) throw new Error('Network response was not ok');
                const profileData = await response.json();
                setReceiverProfile(profileData);
            } catch (error) {
                if (error instanceof Error && error.message === 'Network response was not ok') {
                    logout();
                    navigate('/login');
                }else {
                    setError('Une erreur est survenue lors de la récupération du profil du receveur.');
                    console.error('Error fetching receiver profile:', error);
                }
            }
        };

        const fetchUserPokemons = async () => {
            try {
                const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/pokemons`, {
                    headers: { 'Authorization': `Bearer ${user?.accessToken}` }
                });
                if (!response.ok) throw new Error('Network response was not ok');
                const data = await response.json();
                console.log('Pokemons de l\'utilisateur:', data);
                setUserPokemons(data);
            } catch (error) {
                if (error instanceof Error && error.message === 'Network response was not ok') {
                    logout();
                    navigate('/login');
                }else { 
                    setError('Une erreur est survenue lors de la récupération des Pokémons de l\'utilisateur.');
                    console.error('Error fetching user pokemons:', error);
                }
            }
        };

        const fetchReceiverPokemons = async () => {
            if (!receiverId) return;
            try {
                const response = await fetch(`http://localhost:8000/trainers/${receiverId}/pokemons`, {
                    headers: { 'Authorization': `Bearer ${user?.accessToken}` }
                });
                if (!response.ok) throw new Error('Network response was not ok');
                const data = await response.json();
                console.log('Pokemons du receveur:', data);
                setReceiverPokemons(data);
            } catch (error) {
                if (error instanceof Error && error.message === 'Network response was not ok') {
                    logout();
                    navigate('/login');
                } else {
                    setError('Une erreur est survenue lors de la récupération des Pokémons du receveur.');
                    console.error('Error fetching receiver pokemons:', error);
                }   
            }
        };
    

        fetchUserProfile();
        fetchReceiverProfile();
        fetchUserPokemons();
        fetchReceiverPokemons();
        // if (pokemonIdToTrade) {
        //     setSelectedUserPokemonIds(prev => [...prev, parseInt(pokemonIdToTrade)]);
        // }
        // Sélectionne automatiquement le Pokémon du receveur basé sur l'ID passé dans l'URL
        if (pokemonIdToTrade && receiverId) {
            setSelectedReceiverPokemonIds([parseInt(pokemonIdToTrade)]);
        }
        
    }, [user, pokemonIdToTrade, receiverId, logout, navigate]);


    const handleUserPokemonSelect = (pokemonId: number) => {
        setSelectedUserPokemonIds(prevSelected => {
            if (prevSelected.includes(pokemonId)) {
                return prevSelected.filter(id => id !== pokemonId);
            } else if (prevSelected.length < 6) {
                return [...prevSelected, pokemonId];
            } else {
                setError('Vous ne pouvez sélectionner que 6 Pokémons maximum.');
                return prevSelected;
            }
        });
    };
        

    // const handleUserPokemonSelect = (pokemonId: number) => {
    //     // L'utilisateur ne peut sélectionner qu'un seul Pokémon pour l'échange
    //     setSelectedUserPokemonIds([pokemonId]);
    //   };
      
    const handleReceiverPokemonSelect = (pokemonId: number) => {
        setSelectedReceiverPokemonIds(prevSelected => {
            if (prevSelected.includes(pokemonId)) {
                return prevSelected.filter(id => id !== pokemonId);
            } else if (prevSelected.length < 6) {
                return [...prevSelected, pokemonId];
            } else {
                setError('Vous ne pouvez sélectionner que 6 Pokémons maximum.');
                return prevSelected;
            }
        });
    };
        
    const handleSubmitTrade = async () => {
        if (!receiverId) {
            setError('L\'identifiant du receveur est manquant.');
            return;
        }
    
        if (selectedUserPokemonIds.length === 0 || selectedReceiverPokemonIds.length === 0) {
            setError('Veuillez sélectionner au moins un Pokémon à échanger.');
            return;
        }
    
        try {
            const response = await fetch('http://localhost:8000/trades', {
                method: 'POST',
                headers: {
                    'Authorization': `Bearer ${user?.accessToken}`,
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    pokemonsOfferedIds: selectedUserPokemonIds,
                    pokemonsWantedIds: selectedReceiverPokemonIds,
                    receiverId: parseInt(receiverId)
                })
            });
    
            if (!response.ok) {
                const errorResponse = await response.text();
                setError(`Échec de la soumission : ${errorResponse}`);
                return;
            }
    
            const tradeData = await response.json(); // Récupération des données de l'échange créé
            navigate(`/trades/${tradeData.id}`); // Redirection vers la page de détail de l'échange
        } catch (error) {
            console.error('Error submitting trade:', error);
            setError('Une erreur est survenue lors de la soumission de l\'échange.');
        }
    };
    
    
    return (
        <div className="container mx-auto px-4 py-8">
            <h1 className="text-2xl font-bold mb-6">Proposer un Échange</h1>

            {/* User Information */}
            <div className="mb-4">
                <h2 className="font-semibold text-lg">Vos informations</h2>
                <p>{userProfile?.firstName} {userProfile?.lastName} - {userProfile?.login}</p>
            </div>

            {/* User Pokemon Selection */}
            <div className="grid grid-cols-3 gap-4 mb-4">
                {userPokemons.length > 0 ? userPokemons.map(pokemon => (
                    <div 
                        key={pokemon.id} 
                        onClick={() => handleUserPokemonSelect(pokemon.id)}
                        className={`p-2 border rounded ${isPokemonSelected(pokemon.id, selectedUserPokemonIds) ? 'bg-green-200' : 'bg-white'}`}
                    >
                        {pokemon.species} (Niveau: {pokemon.level})
                    </div>
                )) : <p>Vous n'avez pas de Pokémon.</p>}
            </div>

            {/* Empty Slots if less than 6 Pokemon Selected */}
            {/* ... */}

            {/* Receiver Information */}
            <div className="mb-4">
                <h2 className="font-semibold text-lg">Informations du Receveur</h2>
                <p>{receiverProfile?.firstName} {receiverProfile?.lastName} - {receiverProfile?.login}</p>
            </div>

            {/* Receiver Pokemon Selection */}
            <div className="grid grid-cols-3 gap-4 mb-4">
                {receiverPokemons.length > 0 ? receiverPokemons.map(pokemon => (
                    <div 
                        key={pokemon.id} 
                        onClick={() => handleReceiverPokemonSelect(pokemon.id)}
                        className={`p-2 border rounded ${isPokemonSelected(pokemon.id, selectedReceiverPokemonIds) ? 'bg-blue-200' : 'bg-gray-200'}`}
                    >
                        {pokemon.species} (Niveau: {pokemon.level})
                    </div>
                )) : <p>Le receveur n'a pas de Pokémon.</p>}
            </div>

            {/* Empty Slots if less than 6 Pokemon Selected */}
            {/* ... */}

            {/* Submit Trade Button */}
            {error && <p className="text-red-500 mb-4">{error}</p>}
            <button 
                onClick={handleSubmitTrade}
                className="px-4 py-2 text-white bg-blue-500 rounded hover:bg-blue-600"
            >
                Envoyer la proposition d’échange
            </button>
        </div>

    );
}

export default CreateTradePage;
