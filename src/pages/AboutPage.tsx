// pages/AboutPage.tsx
function AboutPage() {
    return (
      <div className="container mx-auto px-4 py-8">
        <div className="max-w-2xl mx-auto bg-white shadow-lg rounded-lg overflow-hidden p-6">
            <h1 className="text-3xl font-bold text-center mb-4 text-blue-600">À propos</h1>
            <div className="text-center mb-4">
                <svg xmlns="http://www.w3.org/2000/svg" className="h-12 w-12 mx-auto mb-3 text-blue-500" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 11c0 1.656-1.794 3-4 3s-4-1.344-4-3 1.794-3 4-3 4 1.344 4 3z" />
                    <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M12 14v7M16 21H8M9 5c-3 0-5 1.343-5 3v1h10V8c0-1.657-2-3-5-3z" />
                </svg>
            </div>
            <div className="p-4 bg-gray-100 rounded-lg border border-gray-200 shadow-sm">
                      <p className="text-lg text-gray-700 font-semibold text-center italic">
                          With ❤️ by : <span className="text-blue-500">Pape Ibrahima DIAWARA</span>
                      </p>
            </div>
        </div>
  </div>


    );
  }
  
  export default AboutPage;
  