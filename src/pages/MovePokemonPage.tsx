// MovePokemonPage.tsx
import { useState, useEffect } from 'react';
import { useParams, useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { BoxData } from '../Types';
function MovePokemonPage() {
  const { pokemonId } = useParams<{ pokemonId: string }>();
  const { user, logout } = useAuth();
  const navigate = useNavigate();
  const [boxes, setBoxes] = useState<BoxData[]>([]);
  const [selectedBoxId, setSelectedBoxId] = useState<string | number | undefined>(undefined);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState('');

  useEffect(() => {
    // Récupérer toutes les boîtes du dresseur
    const fetchBoxes = async () => {
      try {
        const response = await fetch(`http://localhost:8000/trainers/${user?.trainerId}/boxes`, {
          headers: {
            'Authorization': `Bearer ${user?.accessToken}`
          }
        });

        if (!response.ok) {
          throw new Error('Network response was not ok');
        }

        const data = await response.json();
        setBoxes(data);
      } catch (error) {
        setError('Erreur lors de la récupération des boîtes.');
      } finally {
        setIsLoading(false);
      }
    };

    fetchBoxes();
  }, [user, logout, navigate]);

  const handleMovePokemon = async () => {
    try {
      const response = await fetch(`http://localhost:8000/pokemons/${pokemonId}`, {
        method: 'PATCH',
        headers: {
          'Authorization': `Bearer ${user?.accessToken}`,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ boxId: selectedBoxId }),
      });

      if (!response.ok) {
        throw new Error('Échec du déplacement du Pokémon.');
      }

      navigate(`/boxes/${selectedBoxId}`);
    } catch (error) {
      setError('Erreur lors du déplacement du Pokémon.');
    }
  };

  if (isLoading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }

  return (
    <div className="container mx-auto p-4">
      <h1 className="text-2xl font-bold mb-4">Déplacer le Pokémon</h1>
      <select value={selectedBoxId} onChange={(e) => setSelectedBoxId(e.target.value)}>
        {boxes.map((box) => (
          <option key={box.id} value={box.id}>
            {box.name}
          </option>
        ))}
      </select>
      <button onClick={handleMovePokemon} className="bg-blue-600 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded">
        Déplacer
      </button>
    </div>
  );
}

export default MovePokemonPage;
