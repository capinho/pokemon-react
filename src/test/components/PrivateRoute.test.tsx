import { render } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import PrivateRoute from '../../components/PrivateRoute';
import { AuthProvider } from '../../context/AuthContext';

test('renders PrivateRoute component', () => {
  render(
    <Router>
      <AuthProvider>
        <PrivateRoute />
      </AuthProvider>
    </Router>
  );
});
