import { render, screen } from '@testing-library/react';
import { BrowserRouter as Router } from 'react-router-dom';
import Footer from '../../components/Footer';

test('renders Footer component', () => {
  render(
    <Router>
      <Footer />
    </Router>
  );
  expect(screen.getByText(/À propos/i)).toBeInTheDocument();
});
