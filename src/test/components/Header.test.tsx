//test/components/Header.test.tsx

import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { AuthContext,User } from '../../context/AuthContext'
import Header from '../../components/Header';

describe('Header Component', () => {
  const mockLogout = jest.fn();
  const mockLogin = jest.fn(); // Ajout d'un mock pour login

  const renderHeader = (user: User | null) => {
    render(
      <Router>
        <AuthContext.Provider value={{ user, login: mockLogin, logout: mockLogout }}>
          <Header />
        </AuthContext.Provider>
      </Router>
    );
  };

  it('renders correctly when user is logged in', () => {
    const user = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
    renderHeader(user);

    expect(screen.getByText(/Mes boîtes/)).toBeInTheDocument();
    expect(screen.getByText(/Mes échanges/)).toBeInTheDocument();
    expect(screen.getByText(/Chercher Dresseurs/)).toBeInTheDocument();
    expect(screen.getByText(/Profil/)).toBeInTheDocument();
    expect(screen.getByText(/Déconnexion/)).toBeInTheDocument();
  });

  it('renders correctly when user is not logged in', () => {
    renderHeader(null);

    expect(screen.getByText(/Se connecter/)).toBeInTheDocument();
    expect(screen.getByText(/S'inscrire/)).toBeInTheDocument();
  });

  it('calls logout function when logout button is clicked', () => {
    const user = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
    renderHeader(user);

    const logoutButton = screen.getByText(/Déconnexion/);
    logoutButton.click();
    expect(mockLogout).toHaveBeenCalled();
  });
});
