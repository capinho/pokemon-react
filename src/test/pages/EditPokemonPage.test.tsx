import { render, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import EditPokemonPage from '../../pages/EditPokemonPage';
import { AuthContext, User } from '../../context/AuthContext';
import { BrowserRouter as Router } from 'react-router-dom';

// Mock useNavigate
const mockUseNavigate = jest.fn();

jest.mock('react-router-dom', () => {
    const originalModule = jest.requireActual('react-router-dom');
    return {
        ...originalModule,
        useNavigate: () => mockUseNavigate,
        useParams: () => ({ pokemonId: '1' }),
    };
});

describe('EditPokemonPage Component', () => {
    const mockLogin = jest.fn(); 
    const mockLogout = jest.fn();
    const mockUser: User = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
    let mockFetch: jest.Mock;

    beforeEach(() => {
        mockFetch = jest.fn();
        global.fetch = mockFetch;
        mockUseNavigate.mockClear();
        jest.clearAllMocks();
    });

    const renderEditPokemonPage = (user: User | null) => {
        render(
            <Router>
                <AuthContext.Provider value={{ user, login: mockLogin, logout: mockLogout }}>
                    <EditPokemonPage />
                </AuthContext.Provider>
            </Router>
        );
    };

    it('shows "No Pokémon found" if pokemon data fetch fails', async () => {
        mockFetch.mockResolvedValueOnce({ ok: false });

        renderEditPokemonPage(mockUser);

        await waitFor(() => {
            expect(screen.getByText(/No Pokémon found/)).toBeInTheDocument();
        });
    });
    // it('handles form submission and navigates on success', async () => {
    //     // Mock initial data load
    //     mockFetch.mockResolvedValueOnce({
    //         ok: true,
    //         json: async () => ({
    //             id: 1,
    //             species: 'Pikachu',
    //             name: 'Sparky',
    //             level: 5,
    //             genderTypeCode: 'MALE',
    //             size: 40,
    //             weight: 6,
    //             isShiny: false,
    //             trainerId: 123,
    //             boxId: 15
    //         }),
    //     });
    
    //     renderEditPokemonPage(mockUser);
    
    //     await waitFor(() => expect(screen.getByDisplayValue('Pikachu')).toBeInTheDocument());
    
    //     // Simulate form submission with updated data
    //     fireEvent.change(screen.getByLabelText(/Pokémon Species/), { target: { value: 'Updated Species' } });
    //     fireEvent.change(screen.getByLabelText(/Name/), { target: { value: 'Updated Name' } });
    //     fireEvent.change(screen.getByLabelText(/Level/), { target: { value: 10 } });
    //     fireEvent.change(screen.getByLabelText(/Gender/), { target: { value: 'FEMALE' } });
    //     fireEvent.change(screen.getByLabelText(/Size \(cm\)/), { target: { value: 50 } });
    //     fireEvent.change(screen.getByLabelText(/Weight \(kg\)/), { target: { value: 7 } });
    //     fireEvent.click(screen.getByLabelText(/Shiny/));
        
    //     fireEvent.click(screen.getByText('Save Changes'));
    
    //     // Mock successful form submission
    //     mockFetch.mockResolvedValueOnce({ ok: true });
    
    //     // Wait for the navigation to be called
    //     await waitFor(() => expect(mockUseNavigate).toHaveBeenCalledWith('/pokemons/1'));
    // });
    
});
