import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import CreateBoxPage from '../../pages/CreateBoxPage';
import { AuthContext, User } from '../../context/AuthContext';
import { BrowserRouter as Router } from 'react-router-dom';

describe('CreateBoxPage Component', () => {
  const mockLogout = jest.fn();
  const mockLogin = jest.fn();
  const mockUser: User = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
  let mockFetch: jest.Mock;

  beforeEach(() => {
    mockFetch = jest.fn();
    global.fetch = mockFetch;
    jest.clearAllMocks();
  });

  const renderCreateBoxPage = (user: User | null) => {
    render(
      <Router>
        <AuthContext.Provider value={{ user, login: mockLogin, logout: mockLogout }}>
          <CreateBoxPage />
        </AuthContext.Provider>
      </Router>
    );
  };

  it('allows submission with valid input', async () => {
    renderCreateBoxPage(mockUser);

    const input = screen.getByPlaceholderText('Nom de la boîte');
    fireEvent.change(input, { target: { value: 'New Box' } });
    fireEvent.click(screen.getByText('Créer'));

    mockFetch.mockResolvedValueOnce({ ok: true });

    await waitFor(() => expect(mockFetch).toHaveBeenCalled());
    // Further assertions can be made here, such as checking the fetch call parameters
  });

  it('shows error for empty input', async () => {
    renderCreateBoxPage(mockUser);
    fireEvent.click(screen.getByText('Créer'));
    await waitFor(() => expect(screen.getByText('Please enter a name for the box.')).toBeInTheDocument());
  });

  it('handles fetch error correctly', async () => {
    renderCreateBoxPage(mockUser);

    const input = screen.getByPlaceholderText('Nom de la boîte');
    fireEvent.change(input, { target: { value: 'New Box' } });
    fireEvent.click(screen.getByText('Créer'));

    mockFetch.mockResolvedValueOnce({ ok: false, json: async () => ({ message: 'Error creating the box.' }) });

    await waitFor(() => {
      expect(screen.getByText('Error creating the box.')).toBeInTheDocument();
    });
  });

});
