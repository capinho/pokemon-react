import { render, screen, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import { AuthContext, User } from '../../context/AuthContext';
import BoxesPage from '../../pages/BoxesPage';
import { BrowserRouter as Router } from 'react-router-dom';

describe('BoxesPage Component', () => {
  const mockLogout = jest.fn();
  const mockLogin = jest.fn();
  let mockFetch: jest.Mock;

  beforeEach(() => {
    mockFetch = jest.fn();
    global.fetch = mockFetch;
    jest.clearAllMocks();
  });

  const renderBoxesPage = (user: User | null) => {
    render(
      <Router>
        <AuthContext.Provider value={{ user, login: mockLogin, logout: mockLogout }}>
          <BoxesPage />
        </AuthContext.Provider>
      </Router>
    );
  };

  it('displays loading initially', async () => {
    const user = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
    mockFetch.mockResolvedValueOnce({
      ok: true,
      json: async () => ([]),
    });

    renderBoxesPage(user);

    expect(screen.getByText(/Loading.../)).toBeInTheDocument();
    await waitFor(() => expect(mockFetch).toHaveBeenCalled());
  });

  it('displays boxes on successful fetch', async () => {
    const user = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
    const mockBoxes = [{ id: 1, name: 'Box 1' }];

    // Mock successful fetch
    mockFetch.mockResolvedValueOnce({
      ok: true,
      json: async () => mockBoxes,
    });

    renderBoxesPage(user);

    await waitFor(() => expect(screen.getByText(/Box 1/)).toBeInTheDocument());
  });

  it('handles fetch error correctly', async () => {
    mockFetch.mockResolvedValueOnce({ ok: false });

    const user = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
    renderBoxesPage(user);

    await waitFor(() => {
      expect(mockLogout).toHaveBeenCalled();
      // Here you can check for UI changes or error messages instead of navigation
      // For example: expect(screen.getByText("Error message")).toBeInTheDocument();
    });
  });

  // Add more tests as needed...
});
