import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import { AuthContext, User } from '../../context/AuthContext';
import HomePage from '../../pages/HomePage';
import { useNavigate } from 'react-router-dom';

// Mock the useNavigate hook
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'), 
    useNavigate: jest.fn(),
  }));
  
describe('HomePage Component', () => {
  const mockUser: User = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };

  const renderHomePage = (user: User | null) => {
    return render(
      <Router>
        <AuthContext.Provider value={{ user, login: jest.fn(), logout: jest.fn() }}>
          <HomePage />
        </AuthContext.Provider>
      </Router>
    );
  };

  it('redirects to /boxes if user is authenticated', () => {
    const navigate = jest.fn();
    (useNavigate as jest.Mock).mockImplementation(() => navigate); 
    renderHomePage(mockUser);

    expect(navigate).toHaveBeenCalledWith('/boxes');
  });

  it('redirects to /login if user is not authenticated', () => {
    const navigate = jest.fn();
    (useNavigate as jest.Mock).mockImplementation(() => navigate); 
    renderHomePage(null);

    expect(navigate).toHaveBeenCalledWith('/login');
  });

  it('does not render any UI elements', () => {
    const { container } = renderHomePage(mockUser);
    expect(container).toBeEmptyDOMElement();
  });
});
