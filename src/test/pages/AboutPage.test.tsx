import { render, screen } from '@testing-library/react';
import AboutPage from '../../pages/AboutPage';

test('renders AboutPage component', () => {
  render(<AboutPage />);
  expect(screen.getByText(/À propos/i)).toBeInTheDocument();
});
