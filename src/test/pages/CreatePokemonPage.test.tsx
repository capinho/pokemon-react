//test/components/CreatePokemonPage.test.tsx

import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import CreatePokemonPage from '../../pages/CreatePokemonPage';
import { AuthContext, User } from '../../context/AuthContext';
import { BrowserRouter as Router, useNavigate } from 'react-router-dom';


// Mock useNavigate
jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'), // preserve other exports
    useNavigate: jest.fn(), // mock useNavigate
  }));
  
describe('CreatePokemonPage Component', () => {
  const mockLogout = jest.fn();
  const mockLogin = jest.fn();
  const mockUser: User = { accessToken: 'token', trainerId: 123, expiresAt: 9999999999 };
  let mockFetch: jest.Mock;


  beforeEach(() => {
    mockFetch = jest.fn();
    global.fetch = mockFetch;
    jest.clearAllMocks();
  });

  const renderCreatePokemonPage = (user: User | null) => {
    render(
      <Router>
        <AuthContext.Provider value={{ user, login: mockLogin, logout: mockLogout }}>
          <CreatePokemonPage />
        </AuthContext.Provider>
      </Router>
    );
  };

  it('allows submission with valid input', async () => {
    renderCreatePokemonPage(mockUser);

    fireEvent.change(screen.getByLabelText(/Pokémon Name:/), { target: { value: 'Pikachu' } });
    fireEvent.change(screen.getByLabelText(/Pokémon Species:/), { target: { value: 'Electric Mouse' } });
    fireEvent.change(screen.getByLabelText(/Level:/), { target: { value: 5 } });
    fireEvent.click(screen.getByText('Add Pokémon'));

    mockFetch.mockResolvedValueOnce({ ok: true });

    await waitFor(() => expect(mockFetch).toHaveBeenCalled());
  });

  it('shows error for empty species input', async () => {
    renderCreatePokemonPage(mockUser);
    fireEvent.click(screen.getByText('Add Pokémon'));
    await waitFor(() => expect(screen.getByText('Species is required.')).toBeInTheDocument());
  });

  it('handles fetch error correctly', async () => {
    renderCreatePokemonPage(mockUser);

    fireEvent.change(screen.getByLabelText(/Pokémon Species:/), { target: { value: 'Electric Mouse' } });
    fireEvent.click(screen.getByText('Add Pokémon'));

    mockFetch.mockResolvedValueOnce({ ok: false, json: async () => ({ message: 'Failed to create Pokémon.' }) });

    await waitFor(() => {
      expect(screen.getByText('Failed to create Pokémon. Please try again later.')).toBeInTheDocument();
    });
  });

  it('displays an error message for API failure', async () => {
    renderCreatePokemonPage(mockUser);

    fireEvent.change(screen.getByLabelText(/Pokémon Species:/), { target: { value: 'Electric Mouse' } });
    mockFetch.mockResolvedValueOnce({
      ok: false,
      status: 500,
      json: async () => ({ message: 'Internal Server Error' }),
    });

    fireEvent.click(screen.getByText('Add Pokémon'));

    await waitFor(() => {
      expect(screen.getByText('Failed to create Pokémon. Please try again later.')).toBeInTheDocument();
    });
  });


  it('redirects to the Pokemon detail page on successful creation', async () => {
    const mockNavigate = jest.fn();
    jest.mocked(useNavigate).mockReturnValue(mockNavigate);

    renderCreatePokemonPage(mockUser);

    // Filling out the form fields
    fireEvent.change(screen.getByLabelText(/Pokémon Name:/), { target: { value: 'Pikachu' } });
    fireEvent.change(screen.getByLabelText(/Pokémon Species:/), { target: { value: 'Electric Mouse' } });
    fireEvent.change(screen.getByLabelText(/Level:/), { target: { value: 5 } });
    
    // Mock a successful API response
    mockFetch.mockResolvedValueOnce({
      ok: true,
      json: async () => ({ id: 1 }),
    });

    // Trigger the form submission
    fireEvent.click(screen.getByText('Add Pokémon'));

    // Check if the navigation occurred
    await waitFor(() => {
      expect(mockNavigate).toHaveBeenCalledWith('/pokemons/1');
    });
  });
  

    // Test different response scenarios
    it('handles different non-ok fetch responses', async () => {
        renderCreatePokemonPage(mockUser);
    
        // Simulate user input for various fields
        fireEvent.change(screen.getByLabelText(/Pokémon Name:/), { target: { value: 'Charizard' } });
        fireEvent.change(screen.getByLabelText(/Pokémon Species:/), { target: { value: 'Fire Dragon' } });
        fireEvent.change(screen.getByLabelText(/Level:/), { target: { value: 10 } });
        fireEvent.change(screen.getByLabelText(/Gender:/), { target: { value: 'MALE' } });
        fireEvent.change(screen.getByLabelText(/Size \(in cm\):/), { target: { value: 170 } });
        fireEvent.change(screen.getByLabelText(/Weight \(in kg\):/), { target: { value: 90 } });
        fireEvent.click(screen.getByLabelText(/Is Shiny?/));
    
        // Simulate a 400 Bad Request response
        mockFetch.mockResolvedValueOnce({
          ok: false,
          status: 400,
          json: async () => ({ message: 'Bad Request' }),
        });
    
        fireEvent.click(screen.getByText('Add Pokémon'));
        await waitFor(() => {
          expect(screen.getByText('Failed to create Pokémon. Please try again later.')).toBeInTheDocument();
        });
      });
    

});
