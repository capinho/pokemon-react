import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import '@testing-library/jest-dom';
import LoginPage from '../../pages/LoginPage';
import { AuthContext } from '../../context/AuthContext';
import { BrowserRouter as Router } from 'react-router-dom';
// import { useNavigate } from 'react-router-dom';

// Mock the useNavigate hook
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: jest.fn(),
}));

describe('LoginPage Component', () => {
  const mockLogin = jest.fn();

  const renderLoginPage = (user = null) => {
    return render(
      <Router>
        <AuthContext.Provider value={{ user, login: mockLogin, logout: jest.fn() }}>
          <LoginPage />
        </AuthContext.Provider>
      </Router>
    );
  };

  it('renders input fields and login button', () => {
    renderLoginPage();
    expect(screen.getByPlaceholderText('Username')).toBeInTheDocument();
    expect(screen.getByPlaceholderText('Password')).toBeInTheDocument();
    expect(screen.getByRole('button', { name: 'Login' })).toBeInTheDocument();
  });

  it('updates state on user input', () => {
    renderLoginPage();
    const usernameInput = screen.getByPlaceholderText('Username') as HTMLInputElement;
    const passwordInput = screen.getByPlaceholderText('Password') as HTMLInputElement;
  
    fireEvent.change(usernameInput, { target: { value: 'testuser' } });
    fireEvent.change(passwordInput, { target: { value: 'password123' } });
  
    expect(usernameInput.value).toBe('testuser');
    expect(passwordInput.value).toBe('password123');
  });

  it('handles successful login', async () => {
    const mockSuccessResponse = { accessToken: 'token', trainerId: 123, expiresIn: 3600 };
    global.fetch = jest.fn().mockResolvedValueOnce({
      ok: true,
      json: async () => mockSuccessResponse,
    });
  
    renderLoginPage();
    const usernameInput = screen.getByPlaceholderText('Username');
    const passwordInput = screen.getByPlaceholderText('Password');
    const loginButton = screen.getByRole('button', { name: 'Login' });
  
    fireEvent.change(usernameInput, { target: { value: 'testuser' } });
    fireEvent.change(passwordInput, { target: { value: 'password123' } });
    fireEvent.click(loginButton);
  
    await waitFor(() => {
      expect(mockLogin).toHaveBeenCalledWith(mockSuccessResponse.accessToken, mockSuccessResponse.trainerId, mockSuccessResponse.expiresIn);
    });
  });
  

  it('displays an error message for an unsuccessful login attempt', async () => {
    global.fetch = jest.fn().mockResolvedValueOnce({ ok: false });
  
    renderLoginPage();
    const loginButton = screen.getByRole('button', { name: 'Login' });
  
    fireEvent.click(loginButton);
  
    await waitFor(() => {
      expect(screen.getByText('Invalid username or password.')).toBeInTheDocument();
    });
  });
  

  it('displays an error message when there is a problem processing login', async () => {
    global.fetch = jest.fn().mockRejectedValue(new Error('Network error'));
  
    renderLoginPage();
    const loginButton = screen.getByRole('button', { name: 'Login' });
  
    fireEvent.click(loginButton);
  
    await waitFor(() => {
      expect(screen.getByText('There was an error processing your login.')).toBeInTheDocument();
    });
  });
    
  
});

