//Types.ts
export type Box = {
    id: number;
    name: string;
}

export interface Pokemon {
    id: number;
    species: string;
    name: string;
    level: number;
    genderTypeCode: string;
    isShiny: boolean;
    size: number;
    weight: number;
    boxId: number;
    trainerId: number;
  }
  
  export interface BoxData {
    id: number;
    name: string;
    pokemons: Pokemon[];
  }
  
  export interface UserProfile {
    id: number;
    firstName: string;
    lastName: string;
    login: string;
    birthDate: string;

  }
  

export interface Trade {
  id: number;
  statusCode: 'PROPOSITION' | 'ACCEPTED' | 'DECLINED';
  sender: {
    id: number;
    pokemons: number[]; 
  };
  receiver: {
    id: number;
    pokemons: number[];
  };
}
